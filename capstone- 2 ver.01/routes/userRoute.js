

const express = require("express");

const router = express.Router();

const userController = require("../controllers/userController");

const auth = require ("../auth")



 
router.post("/register", (req, res) => {
  userController.registerUser(req.body)
    .then(resultFromController => res.send(resultFromController));
});



router.post("/login",(req,res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


router.post("/register/admin",(req,res)=>{
	userController.registerAdmin(req.body)
	.then(resultFromController => res.send(resultFromController));
})
router.post("/order",auth.verify, (req,res) =>{
	let data = {
		userId  : auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}
	userController.orders(data).then(resultFromController => res.send(resultFromController));
}) 


router.get("/retrieve",auth.verify,(req,res) =>{
	let data = {
		isAdmin:auth.decode(req.headers.authorization).isAdmin,
		userId: req.body.userId
	}
		userController.getProfile(data).then(resultFromController => res.send(resultFromController));
	});
module.exports = router;