const User = require("../models/Users");
const Product = require("../models/Product")

const bcrypt = require("bcrypt");

const auth = require("../auth");
  

module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password,10)

  });
  return newUser.save()
    .then((user, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });
};

module.exports.loginUser=(reqBody)=>{
	return User.findOne({email:reqBody.email}).then(result =>{
		if (result == null){
			return false

		} else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect){
				return{access:auth.createAccessToken(result)}

			}else{
				return false;
			}
		}
	})
}

module.exports.registerAdmin = (reqBody) => {
  let newUser = new User({
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password,10),
    isAdmin :true
  });
  return newUser.save()
    .then((user, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });
};


module.exports.orders = async (data) => {
  try {
    let user = await User.findById(data.userId);
    let product = await Product.findById(data.productId);

    // Calculate the total quantity
    let quantity = user.orderedProduct.length + 1; // Increment the quantity based on the number of existing orders

    // Calculate the total amount based on the product's price and the quantity
    let productPrice = product.price;
    let totalAmount = productPrice * quantity;

    // Update user's orderedProduct array
    user.orderedProduct.push({
      products: [
        {
          productId: data.productId,
          productName: product.name,
          quantity: quantity
        }
      ],
      totalAmount: totalAmount,
      purchasedOn: new Date()
    });

    // Update user document in the database
    await user.save();

    // Update product's userOrders array
    product.userOrders.push({ userId: data.userId });

    // Update product document in the database
    await product.save();

    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

module.exports.getProfile = (data) => {
  if (data.isAdmin) {
    return User.findById(data.userId).then(result => {
      return result;
    });
  } else {
   
    return Promise.reject(new Error('User is not an admin'));
  }
};